<?php

namespace Helper;

class ParserLogic
{
    public static function parsePersonName($value)
    {
        /**
         * ИНДЕНБАУМ Лев (Indenbaum Léon) 
        */
        $regex = '/^([-\w]+)\s+([-\w]+)\s+\(([^\(\)]+)\).$/ui';
        if (preg_match($regex, $value, $matches) === 1) {
            $lastName      = isset($matches[1]) ? $matches[1] : null;
            $lastNameAlt   = null;
            $firstName     = isset($matches[2]) ? $matches[2] : null;
            $firstNameAlt  = null;
            $middleName    = null;
            $middleNameAlt = null;
            $fullNameAlt   = isset($matches[3]) ? $matches[3] : null;
        } else {
            $regex = '/^([-\w]+)(\s+\(([-\w]+)\))?\s+([-\w]+)(\s+\(([-\w]+)\))?\s+([-\w]+)(\s+\((.+)\))?/ui';
            preg_match($regex, $value, $matches);
            $lastName      = isset($matches[1]) ? $matches[1] : null;
            $lastNameAlt   = isset($matches[3]) ? $matches[3] : null;
            $firstName     = isset($matches[4]) ? $matches[4] : null;
            $firstNameAlt  = isset($matches[6]) ? $matches[6] : null;
            $middleName    = isset($matches[7]) ? $matches[7] : null;
            $middleNameAlt = isset($matches[9]) ? $matches[9] : null;
            $fullNameAlt   = null;
            if (count(preg_split('/\s+/', $middleNameAlt)) > 1) {
                $fullNameAlt = $middleNameAlt;
                $middleNameAlt = null;
            }
        }

        return [
            'LastName'      => trim($lastName),
            'LastNameAlt'   => trim($lastNameAlt),
            'FirstName'     => trim($firstName),
            'FirstNameAlt'  => trim($firstNameAlt),
            'MiddleName'    => trim($middleName),
            'MiddleNameAlt' => trim($middleNameAlt),
            'FullNameAlt'   => trim($fullNameAlt),
        ];
    }

    public static function parseYearLife($value)
    {
        list($birthString, $deathString) = explode('–', $value);
        list($birthDateString, $birthPlaceString) = explode(',', $birthString);
        list($deathDateString, $deathPlaceString) = explode(',', $deathString);

        list($birthDate, $birthDateAlt) = static::parseDateString($birthDateString);
        list($deathDate, $deathDateAlt) = static::parseDateString($deathDateString);

        return [
            'BirthDate'    => trim($birthDate),
            'BirthDateAlt' => trim($birthDateAlt),
            'BirthPlace'   => trim($birthPlaceString),
            'DeathDate'    => trim($deathDate),
            'DeathDateAlt' => trim($deathDateAlt),
            'DeathPlace'   => trim($deathPlaceString),
        ];
    }

    protected static function parseDateString($value)
    {
        $date    = $value;
        $dateAlt = null;
        /**
         * 18 (20) октября 1919
         */
        $regex = '/^(\d+)\s+\((\d+)\)\s+(\w+)\s+(\d{4})$/ui';
        if (preg_match($regex, $value, $matches) === 1) {
            $date = join(' ' , [$matches[1], $matches[3], $matches[4]]);
            $dateAlt = join(' ', [$matches[2], $matches[3], $matches[4]]);
        }
        /**
         * 22 марта (3 апреля) 1919
         */
        $regex = '/^(\d+)\s+(\w+)\s+\((\d+\s+\w+)\)\s+(\d{4})$/ui';
        if (preg_match($regex, $value, $matches) === 1) {
            $date = join(' ' , [$matches[1], $matches[2], $matches[4]]);
            $dateAlt = join(' ' , [$matches[3], $matches[4]]);
        }
        /**
         * 26 декабря 1886 (7 января 1887)
         */
        $regex = '/^(\d+\s+\w+\s+\d{4})\s+\((\d+\s+\w+\s+\d{4})\)$/ui';
        if (preg_match($regex, $value, $matches) === 1) {
            $date = $matches[1];
            $dateAlt = $matches[2];
        }
        return [
            $date,
            $dateAlt,
        ];
    }

    const LIT  = 'Литература';
    const WORK = 'Сочинения';
    const ARCH = 'Архивы';

    public static function parseLiterature($value, $glue)
    {
        $array = explode($glue, $value);
        $literatureArray = [];
        $workArray = [];
        $archiveArray = [];
        foreach ($array as $item) {
            $list = explode(static::LIT.':', $item);
            if (count($list) == 2) {
                $list = $list[1];
                $literatureArray = array_merge($literatureArray, explode(';', $list));
                continue;
            }
            $list = explode(static::WORK.':', $item);
            if (count($list) == 2) {
                $list = $list[1];
                $workArray = array_merge($workArray, explode(';', $list));
                continue;
            }
            $list = explode(static::ARCH.':', $item);
            if (count($list) == 2) {
                $list = $list[1];
                $archiveArray = array_merge($archiveArray, explode(';', $list));
                continue;
            }
        }

        return [
            'Literature' => array_map('trim', $literatureArray),
            'Work'       => array_map('trim', $workArray),
            'Archive'    => array_map('trim', $archiveArray),
        ];
    }

    public static function parseWhoIs($value)
    {
        return [
            'Whois' => $value,
        ];
    }

    public static function parseDescription($value)
    {
        return [
            'Description' => $value,
        ];
    }

    public static function parseAuthor($value)
    {
        // Здесь заменяется символ с кодом 2029
        $value = str_replace(' ', ",", $value);
        $authorArray = explode(',', $value);
        $authorArray = array_filter($authorArray, function ($author) {
            $str = trim($author);
            return !empty($str);
        });
        return [
            'AuthorArray' => $authorArray,
        ];
    }

    public static function parseAsIs($key, $value)
    {
        return [
            $key => $value,
        ];
    }
}
