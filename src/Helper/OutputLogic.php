<?php

namespace Helper;

class OutputLogic
{
    public static function dumpContent($data)
    {
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<Root>\n<Story>{content}\n</Story>\n</Root>";
        $content = '';
        $content .= join('', array_map([OutputLogic::class, 'outputPerson'], $data));
        return strtr($xml, ['{content}' => $content]);
    }

    protected static function outputPerson($person)
    {
        return "\n<Person>"
             . static::outputPersonName($person)
             . static::outputYearLife($person)
             . static::outputWhoIs($person)
             . static::outputDescription($person)
             . static::outputLiterature($person)
             . static::outputAuthor($person)
             . "\n</Person>"
        ;
    }

    protected static function outputPersonName($person)
    {
        if (isset($person['PersonName'])) {
            return join('', [
                "\n<PersonName>" . $person['PersonName'] . "</PersonName>"
            ]);
        }
        return join('', [
            "\n<LastName>"      . $person['LastName']      . "</LastName>",
            "\n<LastNameAlt>"   . $person['LastNameAlt']   . "</LastNameAlt>",
            "\n<FirstName>"     . $person['FirstName']     . "</FirstName>",
            "\n<FirstNameAlt>"  . $person['FirstNameAlt']  . "</FirstNameAlt>",
            "\n<MiddleName>"    . $person['MiddleName']    . "</MiddleName>",
            "\n<MiddleNameAlt>" . $person['MiddleNameAlt'] . "</MiddleNameAlt>",
            "\n<FullNameAlt>"   . $person['FullNameAlt']   . "</FullNameAlt>",
        ]);
    }

    protected static function outputYearLife($person)
    {
        if (!isset($person['BirthDate'])/* and other keys */) {
            return;
        }
        return join('', [
            "\n<BirthDate>"    . $person['BirthDate']    . "</BirthDate>",
            "\n<BirthDateAlt>" . $person['BirthDateAlt'] . "</BirthDateAlt>",
            "\n<BirthPlace>"   . $person['BirthPlace']   . "</BirthPlace>",
            "\n<DeathDate>"    . $person['DeathDate']    . "</DeathDate>",
            "\n<DeathDateAlt>" . $person['DeathDateAlt'] . "</DeathDateAlt>",
            "\n<DeathPlace>"   . $person['DeathPlace']   . "</DeathPlace>",
        ]);
    }

    protected static function outputWhoIs($person)
    {
        return "\n<Whois>" . $person['Whois'] . "</Whois>";
    }

    protected static function outputDescription($person)
    {
        $descrArray = explode("\n", $person['Description']);
        $descr = '<p>' . join("</p>\n<p>", $descrArray) . '</p>';

        return "\n<Description>\n" . $descr ."\n</Description>";
    }

    protected static function outputLiterature($person)
    {
        $literatureArray = array_map(function ($item) {
            return "\n<Literature>" . $item . "</Literature>";
        }, $person['Literature']);
        $workArray = array_map(function ($item) {
            return "\n<Work>" . $item . "</Work>";
        }, $person['Work']);
        $archiveArray = array_map(function ($item) {
            return "\n<Archive>" . $item . "</Archive>";
        }, $person['Archive']);
        return ''
            . join('', $literatureArray)
            . join('', $workArray)
            . join('', $archiveArray)
        ;
    }

    protected static function outputAuthor($person)
    {
        return join('', array_map(function ($author) {
            return "\n<Author>" . $author . "</Author>";
        }, $person['AuthorArray']));
    }
}
