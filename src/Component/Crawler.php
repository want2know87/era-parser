<?php

namespace Component;

use Symfony\Component\DomCrawler\Crawler as SymfonyCrawler;

class Crawler extends SymfonyCrawler
{
    public function textValue($selector)
    {
        try {
            $return = $this->filter($selector)->first()->text();
        } catch (\InvalidArgumentException $e) {
            $return = null;
        }
        return $return;
    }

    public function arrayAsTextValue($selector, $glue = "\n")
    {
        return join($glue, $this->filter($selector)->extract(['_text']));
    }
}
