<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

class ScanCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('scan')
            ->setDescription('Scans directory for files to convert')
            ->setHelp(<<<'EOF'
You should put your xml files under <info>`in`</info> directory:

  <info>php %command.full_name%</info>
EOF
            )
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $this->getApplication()->find('convert');

        $files = array_diff(scandir(__DIR__.'/../../in'), array('..', '.', '.gitignore'));
        foreach ($files as $file) {
            $convertInput = new ArrayInput([
                'command' => 'convert',
                'path'    => "in/$file",
            ]);
            $returnCode = $command->run($convertInput, $output);
        }
    }
}
