<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Component\Crawler;
use Helper\ParserLogic;
use Helper\OutputLogic;

class ConvertCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('convert')
            ->addArgument('path', InputArgument::REQUIRED, 'Path')
            ->setDescription('Converts xml file to more detailed xml file')
            ->setHelp(<<<'EOF'
You should provide filepath:

  <info>php %command.full_name% example/in.xml</info>
EOF
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $filename = basename($path);
        $xml = file_get_contents(realpath($path));
        $crawler = new Crawler($xml);
        $persons = $crawler->filter('Person');
        $data = [];
        $persons->each(function (Crawler $person) use (&$data) {
            $personName = $person->textValue('Person_name');
            $yearLife = $person->textValue('Year_life');
            $whoIs = $person->textValue('Whois');
            $description = $person->arrayAsTextValue('Description');
            $author = $person->textValue('Author');
            $glue = 'glue';
            $literature = $person->arrayAsTextValue('Literature', $glue);
            $data[] = ParserLogic::parsePersonName($personName)
                + ParserLogic::parseYearLife($yearLife)
                + ParserLogic::parseWhoIs($whoIs)
                + ParserLogic::parseDescription($description)
                + ParserLogic::parseLiterature($literature, $glue)
                + ParserLogic::parseAuthor($author)
            ;
        });
        $result = file_put_contents(__DIR__."/../../dest/$filename", OutputLogic::dumpContent($data));
    }
}
